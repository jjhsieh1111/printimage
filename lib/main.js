// This is an active module of the JJ_Hsieh (2) Add-on

'use strict';

// super powerful Component script.  Allows access to everything a browser can access.
// As noted by Firefox, this is experimental, and may change or be removed in the future.
var {Cc, Ci, Cm, Cr, Cu} = require("chrome");

// get OS temp files directory (/tmp)
var temp_dir = require('sdk/system').pathFor('TmpD');
    
// preference set
var simple_prefs = require("simple-prefs");

// "data" is supplied by the "self" module
var data = require("sdk/self").data;

// i10n
var _ = require("sdk/l10n").get;

exports.main = function() {};

//
// Add Context menu when right-click on an image
//

var cm = require("sdk/context-menu");

cm.Item({
  label: _("auto_print_image_id"),
  context: cm.SelectorContext("img"),
  //contentScript: 'self.on("click", function (node, data) {' +
  //               '  self.postMessage(node.src);' +
  //               '});',
  contentScriptFile: [data.url("print_image.js")],
  onMessage: function (imgSrc) {
    console.log('clicked: ' + imgSrc);

    // Save image
    var local_file = temp_dir + "/" + "PrintImage-" + GetUUID();
    DownloadImage(imgSrc, local_file);
  }
});

function GetUUID () 
{
    var uuidGenerator = Cc["@mozilla.org/uuid-generator;1"].getService(Ci.nsIUUIDGenerator);
    var uuid = uuidGenerator.generateUUID();
    var uuidString = uuid.toString();
    return uuidString.slice(1, -1); // remove '{' and '}'
}

function PrintImage (file_name)
{
    // Run the process.
    // If first param is true, calling thread will be blocked until
    // called process terminates.
    // Second and third params are used to pass command-line arguments
    // to the process.
    var args = [];
    
    var parameters = simple_prefs.prefs['parameters'].trim().split(" ");
    for (var i = 0; i < parameters.length ; i++)
    {
        if (parameters[i] != "") {
            args.push(parameters[i]);
        }
    }
    args.push (file_name);
    
    // create an nsIFile for the executable
    var externalTool = Cc["@mozilla.org/file/local;1"].createInstance(Ci.nsILocalFile); 
    externalTool.initWithPath(simple_prefs.prefs['external-tool'].trim());
    
    // create an nsIProcess
    var process = Cc["@mozilla.org/process/util;1"].createInstance(Ci.nsIProcess); 
    process.init(externalTool);

    console.log("Printing image with the following command:");
    console.log (simple_prefs.prefs['external-tool'] + " " + args.join(" "));
    process.run(false, args, args.length);
}

function DownloadImage (aURLToDownload, aSaveToFile)
{
    console.log("Saving from: " + aURLToDownload);
    console.log("Saving as: " + aSaveToFile);
    try {

        // download from: aURLToDownload
        var downloadURI = Cc["@mozilla.org/network/io-service;1"].getService(Ci.nsIIOService).newURI(aURLToDownload, null, null);
        
        // download destination
        var outputFile = Cc["@mozilla.org/file/local;1"].createInstance(Ci.nsILocalFile); 
        outputFile.initWithPath(aSaveToFile)

        var persist = Cc["@mozilla.org/embedding/browser/nsWebBrowserPersist;1"].createInstance(Ci.nsIWebBrowserPersist);

        persist.progressListener = {
            onStateChange: function(aWebProgress, aRequest, aFlag, aStatus) {
                // If you use myListener for more than one tab/window, use
                // aWebProgress.DOMWindow to obtain the tab/window which triggers the state change
                if (aFlag & Ci.nsIWebProgressListener.STATE_START) {
                    // This fires when the load event is initiated
                    console.log("STATE_START");
                }
                if (aFlag & Ci.nsIWebProgressListener.STATE_STOP) {
                    // This fires when the load finishes
                    console.log("STATE_STOP");
                    console.log("Download complete, print the image");
                    PrintImage (aSaveToFile);
                }
            },
            onProgressChange: function(aWebProgress, aRequest, aCurSelfProgress, aMaxSelfProgress, aCurTotalProgress, aMaxTotalProgress) {
                var percentComplete = (aCurTotalProgress/aMaxTotalProgress)*100;
                console.log("aCurSelfProgress = " + aCurSelfProgress);
                console.log("aMaxSelfProgress = " + aMaxSelfProgress);
                console.log("aCurTotalProgress = " + aCurTotalProgress);
                console.log("aMaxTotalProgress = " + aMaxTotalProgress);
                console.log(aSaveToFile + " " + roundToTwo(percentComplete) +"%");
            }
        };
        
        persist.persistFlags = persist.PERSIST_FLAGS_REPLACE_EXISTING_FILES;

        // https://developer.mozilla.org/en-US/docs/XPCOM_Interface_Reference/nsIWebBrowserPersist#saveURI
        persist.saveURI(downloadURI, null, null, null, "", outputFile, null);     
    } catch (e) {
        console.log(e);
    }
}

function roundToTwo(num) {    
    return +(Math.round(num + "e+2")  + "e-2");
}
